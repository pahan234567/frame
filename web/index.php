<?php

use Framework\Route\Route;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Diactoros\ServerRequestFactory;
use Zend\Diactoros\Response\SapiEmitter;
use Models\Database;

chdir(dirname(__DIR__));
require 'vendor/autoload.php';

$db = new Database();

### Инициализация запроса
$request = ServerRequestFactory::fromGlobals();

### Action Route
//$action = Route::start($request);


$response = new \Zend\Diactoros\Response\HtmlResponse('sdfsdf');
//### Postprocessing
//if ($action) {
//    $response = $action($request);
//} else {
//    $response = new JsonResponse(['error' => 'Undefined page'], 404);
//}
$response = $response->withHeader('X-Developer', 'BeeJee');

### Sending

$emmiter = new SapiEmitter();
$emmiter->emit($response);
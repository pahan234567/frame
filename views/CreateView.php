<?php

use Framework\View\View;
use Models\Session;

if (Session::hasFlash('success_add')) {
    $flash_html = '<div class="alert alert-success">
  <strong>Успех!</strong> '. Session::getFlash('success_add') .'
</div>';
}

$modal_preview_html = '<!-- Модальное окно -->  
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal1" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Предпросмотр</h4>
      </div>
      <div class="modal-body">
      <form id="myform" method="POST" action="/create">
            <div class="form-group">
                <label for="inputUser">Имя пользователя</label>
                <input type="text" class="form-control" class="username"  id="username"  readonly="true" placeholder="Введите имя пользователя">
         </div>
          <div class="form-group">
                <label for="InputEmail">Email адрес</label>
                <input type="email" class="form-control" class="email" id="email"  readonly="true"  placeholder="Введите e-mail"> 
         </div>
         <div class="form-group">
             <label for="inputDescr">Текст задачи</label>
             <textarea class="form-control" class="description" id="description" readonly="true" rows="3"></textarea>
         </div>
         <img id=\'myImage\' src="public/images/">
        
</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
        <button type="button" class="btn btn-primary js-add">Создать</button>
      </div>
    </div>
  </div>
</div>';

$modal_photo_html = '  
            <!-- Модальное окно -->  
<div class="modal fade bd-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Загрузка изображения</h4>
      </div>
      <div class="modal-body">
       <!-- Content -->
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <!-- <h3 class="page-header">Demo:</h3> -->
            <div class="img-container">
                <img src="" alt="Загрузите изображение">
            </div>
        </div>
    </div>
    <div class="row" id="actions">
        <div class="col-md-9 docs-buttons">
            <!-- <h3 class="page-header">Toolbar:</h3> -->
            <div class="btn-group">
                <label class="btn btn-primary btn-upload" for="inputImage" title="Выберите изображение">
                    <input type="file" class="sr-only" id="inputImage" name="file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                    <span class="docs-tooltip" data-toggle="tooltip" title="Выберите изображение">
 <span class="fa fa-upload">Выберите изображение</span>
 </span>
                </label>
            </div>
            <div class="btn-group btn-group-crop">
                <a href="javascript:" class="btn btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;width&quot;: 160, &quot;height&quot;: 90 }">
                    Загрузить
                </a>
            </div>
            <!-- Show the cropped image in modal -->
            <div class="modal fade docs-cropped" id="getCroppedCanvasModal" role="dialog" aria-hidden="true" aria-labelledby="getCroppedCanvasTitle" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="getCroppedCanvasTitle">Cropped</h4>
                        </div>
                        <div class="modal-body"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <a class="btn btn-primary" id="download" href="javascript:void(0);" download="cropped.jpg">Download</a>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal -->
        </div>
        <div class="col-md-3 docs-toggles">
        </div><!-- /.docs-toggles -->
    </div>
</div>
      </div>
    </div>
  </div>
</div>';

return $flash_html . $modal_photo_html . $modal_preview_html . '<div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Добавить задачу</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                <div class="col-xs-1"></div>
                <div class="col-xs-10">

       <form id="myform" method="POST" action="/create">
            <div class="form-group">
                <label for="inputUsername">Имя пользователя</label>
                <input type="text" class="form-control" name="username" id="inputUsername" placeholder="Введите имя пользователя">
         </div>
          <div class="form-group">
                <label for="InputEmail">Email адрес</label>
                <input type="email" class="form-control" name="email" id="inputEmail"  placeholder="Введите e-mail"> 
         </div>
         <div class="form-group">
             <label for="inputDescription">Текст задачи</label>
             <textarea class="form-control" name="description" id="inputDescription" rows="3"></textarea>
         </div>
         <div class="form-group">
             <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                Загрузить фотографию
            </button>
         </div>
<input type="submit" class="btn btn-primary js-ok" id="jsok" name="okbutton" value="Добавить" />
 <!-- Кнопка пуска модальное окно -->  
<button type="button" class="btn btn-primary btn-lg js-preview" data-toggle="modal" data-target="#modal">
  Предпросмотр
</button>
</form>
       </div>
</div>
                </div>
      
                </div>
            </div>
    </div>
</div>
<!-- Подключим стили -->
<link rel="stylesheet" href="public/css/plugin/cropper/cropper.css">

<!-- Подключим js -->
<script src="public/js/plugin/cropper/cropper.js"></script>
<script src="public/js/plugin/cropper/main.js"></script>
'


    . View::endHtml();
?>



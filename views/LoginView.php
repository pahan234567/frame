<?php

use Framework\View\View;
use Models\Session;

if (Session::hasFlash('success_login')) {
    $flash_html = '<div class="alert alert-success">
  <strong>Успех!</strong> '. Session::getFlash('success_login') .'
</div>';
}

return '<div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Авторизация</h3>
            </div>
            <div class="panel-body">
                '. $flash_html .'
            </div>
    </div>
</div>'

    . View::endHtml();
?>
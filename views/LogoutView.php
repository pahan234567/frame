<?php

use Framework\View\View;
use Models\Session;

if (Session::hasFlash('success_logout')) {
    $flash_html = '<div class="alert alert-success">
  <strong>Успех!</strong> '. Session::getFlash('success_logout') .'
</div>';
}

return '<div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Выход из системы</h3>
            </div>
            <div class="panel-body">
                '. $flash_html .'
            </div>
    </div>
</div>'

    . View::endHtml();
?>
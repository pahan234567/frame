<?php

use Framework\View\View;


// сформируем задачи
$tasks_html = '';

foreach ($tasks as $task) {
    if($task->status == 0) {
        $status = '<button type="button" class="btn btn-primary btn-xs">В процессе</button>';
    }

    if($task->status == 1){
        $status = '<button type="button" class="btn btn-success btn-xs">Выполнена</button>';
    }



    $tasks_html .= '<tr>
                <th>'. $task->username .'</th>
                <th>'. $task->email .'</th>
                <th>' . $task->description . '</th>
                <th><img src="public/images/' . $task->img . '" style="width: 100px"></th>
                <th>' . $status. '</th>
                <th>
                <a href="/edit/'. $task->id . '" class="btn btn-primary btn-sm">Ред</a>
                <a href="/delete/'. $task->id . '" class="btn btn-danger btn-sm">Удалить</a>
                </th>
            </tr>';
}

return '
<div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Задачник (BeeJee)</h3>
            </div>
            <div class="panel-body">
<table id="example"  data-page-length=\'3\' class="display" style="width:100%">
        <thead>
            <tr>
                <th>Имя пользователя</th>
                <th>E-mail</th>
                <th>Текст задачи</th>
                <th>Картинка</th>
                <th>Статус</th>
                <th>Управление</th>
            </tr>
        </thead>
        <tbody>
            '. $tasks_html .'
        </tbody>
        <tfoot>
           <tr>
                <th>Имя пользователя</th>
                <th>E-mail</th>
                <th>Текст задачи</th>
                <th>Картинка</th>
                <th>Статус</th>
                <th>Управление</th>
            </tr>
        </tfoot>
    </table>
    </div>
    </div>
'

    . View::endHtml();


?>
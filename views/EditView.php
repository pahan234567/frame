<?php

use Framework\View\View;
use Models\Session;

if (Session::hasFlash('success_edit')) {
    $flash_html = '<div class="alert alert-success">
  <strong>Успех!</strong> '. Session::getFlash('success_edit') .'
</div>';
}

return $flash_html . '<form id="myform" method="POST">
            <div class="form-group">
                <label for="inputUsername">Имя пользователя</label>
                <input type="text" class="form-control" name="username" value="'.$task->username .'" id="inputUsername" placeholder="Введите имя пользователя">
         </div>
          <div class="form-group">
                <label for="InputEmail">Email адрес</label>
                <input type="email" class="form-control" name="email" value="'.$task->email .'" id="inputEmail"  placeholder="Введите e-mail"> 
         </div>
         <div class="form-group">
             <label for="inputDescription">Текст задачи</label>
             <textarea class="form-control" name="description"  id="inputDescription" rows="3">'.$task->description .'</textarea>
         </div>
        <div class="form-group">
         <label for="inputDescription">Статус</label>
        <select id="formaddtracks-genre" class="form-control" name="status">
                <option value="0">В процессе</option>
                <option value="1">Готово</option>
        </select>
       </div>
<input type="submit" class="btn btn-primary"  name="okbutton" value="Изменить" />
</form>'

    . View::endHtml();


?>
<?php

use Framework\View\View;
use Models\Session;

if (Session::hasFlash('success_delete')) {
    $flash_html = '<div class="alert alert-success">
  <strong>Успех!</strong> '. Session::getFlash('success_delete') .'
</div>';
}

return '<div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Результат</h3>
            </div>
            <div class="panel-body">
                '. $flash_html .'
                <a href="/" class="btn btn-primary">Перейти назад</a>
            </div>
    </div>
</div>'

    . View::endHtml();


?>
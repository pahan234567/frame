<?php

namespace Controllers;

use Framework\Controller\Controller;
use http\Env\Request;
use http\Env\Response;
use Models\Session;
use Models\User;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Models\Task;
use Zend\Diactoros\Response\JsonResponse;

class MainController extends Controller
{
    /**
     * Главная страница сервиса "Задачник"
     *
     * @param ServerRequestInterface $request
     * @return \Closure|void
     */
    public function actionIndex(ServerRequestInterface $request)
    {
        // получим все задачи
        $tasks = Task::all();

        $data = [
           'tasks' => $tasks
        ];

       return $action = function (ServerRequestInterface $request) use($data) {
            $name = $request->getQueryParams()['name'] ?? 'Guest';
            return new HtmlResponse('dsf');
        };
    }

    /**
     * Страница "О сайте"
     *
     * @return \Closure
     */
    public function actionAbout()
    {
        return $action = function (ServerRequestInterface $request) {
            return new HtmlResponse($this->view->generate('AboutView.php','Layout.php'));
        };
    }

    /**
     * Страница "Контакты"
     *
     * @return \Closure
     */
    public function actionContact()
    {
        return $action = function (ServerRequestInterface $request) {
            return new HtmlResponse($this->view->generate('ContactView.php','Layout.php'));
        };
    }

    /**
     * Страница "Создание задачи"
     *
     * @return \Closure
     */
    public function actionCreate(ServerRequestInterface $request)
    {
        $post = $request->getParsedBody($request->getBody());

        if (isset($post['username'])) {
            session_start();

            $task = Task::add($post['username'], $post['email'], $post['description'], $_SESSION['image1']);

            if (isset($task)) {
                Session::setFlash('success_add', 'Задача успешно создана!');
            }
        }

        return $action = function (ServerRequestInterface $request) {
            return new HtmlResponse($this->view->generate('CreateView.php','Layout.php'));
        };
    }

    /**
     * Метод редактирует задачу
     *
     * @param ServerRequestInterface $request
     * @return \Closure
     */
    public function actionEdit(ServerRequestInterface $request)
    {
        session_start();

        if (isset($_SESSION['admin']) && $_SESSION['admin'] == true) {

            $post = $request->getParsedBody($request->getBody());
            $id = $request->getAttribute('id');
            $task = Task::find($id);

            if (isset($post['username'])) {

                $task->username = $post['username'];
                $task->email = $post['email'];
                $task->description = $post['description'];
                $task->status = $post['status'];
                $task->save();
                Session::setFlash('success_edit','Задача успешно отредактирована');
            }


            $data = [
                'task' => $task
            ];


            return $action = function (ServerRequestInterface $request) use($data){
                return new HtmlResponse($this->view->generate('EditView.php','Layout.php',$data));
            };
        } else {
            return $action = function (ServerRequestInterface $request) {
                return new JsonResponse(['error' => 'Forbidden'], 403);;
            };
        }
    }

    /**
     * Метод авторизации пользователя
     *
     * @param ServerRequestInterface $request
     * @return \Closure
     */
    public function actionLogin(ServerRequestInterface $request)
    {
        $post = $request->getParsedBody($request->getBody());

        if ($post['login']) {
            $user = User::where('username', $post['login'])->where('password', $post['password'])->count();

            if ($user == 1) {
                session_start();

                $_SESSION['admin'] = true;
                Session::setFlash('success_login', 'Вы успешно авторизировались');
            } else {
                Session::setFlash('success_login', 'Не правильный логин или пароль');
            }
        }

        return $action = function (ServerRequestInterface $request) {
            return new HtmlResponse($this->view->generate('LoginView.php','Layout.php'));
        };
    }

    /**
     * Метод для выхода из системы
     *
     * @return \Closure
     */
    public function actionLogout()
    {
        session_start();

        unset($_SESSION['admin']);

        Session::setFlash('success_logout', 'Вы успешно вышли из системы');

        return $action = function (ServerRequestInterface $request) {
            return new HtmlResponse($this->view->generate('LogoutView.php','Layout.php'));
        };
    }


    /**
     * Ajax метод, который загружает изображение на сервер
     *
     *
     * @return \Closure
     */
    public function actionAjaxAddImg()
    {
        session_start();

        if ( isset($_POST["image1"]) && !empty($_POST["image1"]) ) {
            $sizes = [
                '200x199',
                '32x32'
            ];
            $path = $_SERVER['DOCUMENT_ROOT'] . "/public/images/";

            if (!is_dir($path)) {
                mkdir($path, 0755);
            }

            for ($i = 1; $i < 3; $i++) {
                $data = $_POST['image' . $i];
                $uri =  substr($data,strpos($data,",")+1);
                $image_name = time() . '_' . $sizes[$i-1] . '.png';
                $_SESSION['image' . $i] = $image_name;
                $file = $_SERVER['DOCUMENT_ROOT'] . '/public/images/' . $image_name;
                file_put_contents($file, base64_decode($uri));
            }
        }

        $result = [
            'response' => 'success',
            'img'      => $_SESSION['image1']
        ];

        return $action = function (ServerRequestInterface $request) use($result){
            return new JsonResponse($result);
        };
    }

    /**
     * Метод удаляет задачу
     *
     * @param ServerRequestInterface $request
     */
    public function actionDelete(ServerRequestInterface $request)
    {
        session_start();
        if (isset($_SESSION['admin']) && $_SESSION['admin'] == true) {
            if ($request->getAttribute('id')) {
                $id = $request->getAttribute('id');
                $task = Task::find($id);
                $deleted = $task->delete();
            }

            Session::setFlash('success_delete','Задача усппешно удалена');

            return $action = function (ServerRequestInterface $request)  {
                return new HtmlResponse($this->view->generate('DeleteView.php','Layout.php'));
            };
        } else {
            return $action = function (ServerRequestInterface $request) {
                return new JsonResponse(['error' => 'Forbidden'], 403);;
            };
        }

    }

}
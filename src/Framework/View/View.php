<?php

namespace Framework\View;

class View
{
    //public $template_view; // здесь можно указать общий вид по умолчанию.

    function generate($content, $layout, $data = null)
    {
       return $this->layout($content, $data);

    }

    /**
     * Layout шаблона
     *
     * @param $content
     * @param $data
     * @return string
     */
    function layout($content, $data)
    {
        if(is_array($data)) {
            // преобразуем элементы массива в переменные
            extract($data);
        }

        session_start();

        if (isset($_SESSION['admin']) && $_SESSION['admin'] == true) {
            $form_login = '
<div class="navbar-collapse collapse">
<form class="navbar-form navbar-right">
<div class="form-group">
                                  <a href="/logout" class="btn btn-primary">Выйти</a>
                                </div>

 </form>
</div>
';
        } else {
            $form_login = '<div class="navbar-collapse collapse">
                            <form class="navbar-form navbar-right" method=\'post\' action=\'/login\'>
                                <div class="form-group">
                                    <input type="text" name=\'login\' placeholder="Логин" class="form-control">
                                </div>
                                <div class="form-group">
                                    <input type="password" name=\'password\' placeholder="Пароль" class="form-control">
                                </div>
                            <button type="submit" class="btn btn-success">Войти</button>
                            </form>
                    </div>';
        }



        return "
                <!DOCTYPE HTML>
                <html lang=\"ru-RU\">
                    <head>
                        <title>Задачник</title>
                         <meta charset=\"utf-8\">
                         <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
                         <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
                         <meta name=\"description\" content=\"\">
                         <meta name=\"author\" content=\"\">
                <script
            src=\"http://code.jquery.com/jquery-2.2.4.js\"
            integrity=\"sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=\"
            crossorigin=\"anonymous\"></script>
                
                <!-- Latest compiled and minified CSS -->
                <link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css\">
                <link rel=\"stylesheet\" href=\"//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css\">
                <!-- Optional theme -->
                <link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css\">
                <!-- Latest compiled and minified JavaScript -->
                <script src=\"//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js\"></script>
                <script src=\"//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js\"></script>
                <script src=\"public/js/tasks.js\"></script>
           
               </head>
               <body>
               <div class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            </button>
                            <a class=\"navbar-brand\" href=\"\\\">Задачник (BeeJee)</a>
                            <a class=\"navbar-brand\" href=\"\create\">Добавить задачу</a>
                            <a class=\"navbar-brand\" href=\"about\">О сервисе</a>
                            <a class=\"navbar-brand\" href=\"contact\">Контакты</a>
                        </div>
                    ". $form_login ."
                </div>
            </div>
            <div class=\"jumbotron\">
                <div class=\"container\">
                    <h1>Здравствуйте, гость!</h1>
                    <p>Вы попали на онлайн сервис \"Задачник\", разработаный в рамках тестового задания от BeeJee!</p>
                </div>
            </div>
          <div class='container'>

" . include $_SERVER['DOCUMENT_ROOT'] . "/views/" .  "$content ";
    }


    /**
     * В каждом виде добавляется конец html разметки
     *
     * @return string
     */
    public static function endHtml()
    {
      return
          '
            </container>
            </body>
           </html>
           ';
    }
}
<?php

namespace Framework\Route;

use Controllers\MainController;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;

class Route
{
    public static function start(ServerRequestInterface $request)
    {
        $action = null;
        $path = $request->getUri()->getPath();

        if ($path === '/') {
            $contoller = new MainController();
            $action = $contoller->actionIndex($request);
        }  elseif ($path === '/edit') {

            $action = function (ServerRequestInterface $request) {
                return new HtmlResponse('Отредактируй меня');
            };

        }  elseif ($path === '/view') {

            $action = function (ServerRequestInterface $request) {
                return new HtmlResponse('Посмотри на меня!');
            };

        } elseif ($path === '/about') {

            $contoller = new MainController();
            $action = $contoller->actionAbout($request);

        } elseif ($path === '/logout') {

            $contoller = new MainController();
            $action = $contoller->actionLogout($request);

        } elseif ($path === '/contact') {

            $contoller = new MainController();
            $action = $contoller->actionContact($request);

        } elseif ($path === '/create') {

            $contoller = new MainController();
            $action = $contoller->actionCreate($request);

        }elseif ($path === '/ajax-add-img') {

            $contoller = new MainController();
            $action = $contoller->actionAjaxAddImg($request);

        } elseif (preg_match('#^/delete/(?P<id>\d+)$#i', $path, $matches)) {
            $id = $matches['id'];
            $request = $request->withAttribute('id' , $matches['id']);
            $contoller = new MainController();
            $action = $contoller->actionDelete($request);
        } elseif (preg_match('#^/edit/(?P<id>\d+)$#i', $path, $matches)) {
            $id = $matches['id'];
            $request = $request->withAttribute('id' , $matches['id']);
            $contoller = new MainController();
            $action = $contoller->actionEdit($request);
        } elseif ($path === '/login') {

            $contoller = new MainController();
            $action = $contoller->actionLogin($request);

        } else {

            $action = function (ServerRequestInterface $request) {
                return new JsonResponse(['error' => 'Undefined page'], 404);
            };
        }

        return $action;
    }
}
<?php

namespace Framework\Controller;

use Framework\View\View;
use Psr\Http\Message\ServerRequestInterface;

class Controller
{
    public $model;
    public $view;

    function __construct()
    {
        $this->view = new View();
    }

    function actionIndex(ServerRequestInterface $request)
    {

    }
}
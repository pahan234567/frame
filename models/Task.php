<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
   protected $table = 'task';
   protected $fillable = ['username', 'email', 'description', 'img','updated_at','status'];

    /**
     * Метод добавляет в БД новую задачу
     *
     * @param $username
     * @param $email
     * @param $description
     * @param $img
     * @return Task
     */
   public static function add($username, $email, $description, $img):Task
   {
      $data = [
          'username'    => $username,
          'email'       => $email,
          'description' => $description,
          'img'         => $img,
          'status'      => 0
      ];

      $task = self::create($data);

      return $task;
   }

}
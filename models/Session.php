<?php

namespace Models;

class Session
{
    public static function setFlash($name_flash, $message)
    {
        session_start();
        $_SESSION[$name_flash] = $message;
    }

    public static function hasFlash($name_flash)
    {
        session_start();

        if (isset($_SESSION[$name_flash])) {
            return true;
         } else {
            return false;
        }
    }

    public static function getFlash($name_flash)
    {
        session_start();

        $message = $_SESSION[$name_flash];
        unset($_SESSION[$name_flash]);

        return $message;
    }
}